﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Authorization
{

    /// <inheritdoc />
    /// <summary>
    ///     Requires that at least one given policy must be met.
    /// </summary>
    public class PolicyAuthorizationRequirement : IAuthorizationRequirement
    {

        /// <summary>
        ///     Collection of policies to test against.
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public IEnumerable<string> Policies { get; }

        /// <summary>
        ///     Initialises a new <see cref="PolicyAuthorizationRequirement" /> instance with a provided list of 
        ///     policies.
        /// </summary>
        /// <param name="policies">List of policies that must be met.</param>
        public PolicyAuthorizationRequirement([NotNull] [ItemNotNull] IEnumerable<string> policies)
        {
            if (policies == null) {
                throw new ArgumentNullException(nameof(policies));
            }

            // ReSharper disable once PossibleMultipleEnumeration
            Policies = policies.ToList()?.AsReadOnly()
                       // ReSharper disable once PossibleMultipleEnumeration
                       ?? throw RuntimeInconsistencyException.FromUnexpectedNull(() => policies.ToList().AsReadOnly());

            if (!Policies.Any()) {
                throw new InvalidOperationException("Provided list of policies is empty.");
            }
        }

    }

}
