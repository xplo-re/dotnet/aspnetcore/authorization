﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Authorization
{

    /// <inheritdoc />
    /// <summary>
    ///     Requires that at least one given requirement must be met.
    /// </summary>
    public class DisjunctiveAuthorizationRequirement : IAuthorizationRequirement
    {

        /// <summary>
        ///     Collection of requirements to verify.
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public IEnumerable<IAuthorizationRequirement> Requirements { get; }

        /// <summary>
        ///     Initialises a new <see cref="DisjunctiveAuthorizationRequirement" /> instance with a provided list of 
        ///     requirements.
        /// </summary>
        /// <param name="requirements">List of policies that must be met.</param>
        public DisjunctiveAuthorizationRequirement(
            [NotNull] [ItemNotNull] IEnumerable<IAuthorizationRequirement> requirements)
        {
            if (requirements == null) {
                throw new ArgumentNullException(nameof(requirements));
            }

            // ReSharper disable once PossibleMultipleEnumeration
            Requirements = requirements.ToList()?.AsReadOnly()
                           ?? throw RuntimeInconsistencyException.FromUnexpectedNull(
                               // ReSharper disable once PossibleMultipleEnumeration
                               () => requirements.ToList().AsReadOnly());

            if (!Requirements.Any()) {
                throw new InvalidOperationException("Provided list of requirements is empty.");
            }
        }

    }

}
