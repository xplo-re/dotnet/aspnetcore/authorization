﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Authorization
{

    /// <inheritdoc />
    /// <summary>
    ///     Requires the existence of at least one of the specified claim types.
    /// </summary>
    public class ClaimTypeAuthorizationRequirement : IAuthorizationRequirement
    {

        /// <summary>
        ///     Claim types of which (at least) one must be present.
        /// </summary>
        [NotNull]
        [ItemNotNull]
        public IEnumerable<string> ClaimTypes { get; }

        /// <summary>
        ///     Initialises a new <see cref="ClaimTypeAuthorizationRequirement" /> instance with a provided list of 
        ///     accepted claim types.
        /// </summary>
        /// <param name="claimTypes">List of accepted claim types of which at least one must be present.</param>
        public ClaimTypeAuthorizationRequirement([NotNull] [ItemNotNull] IEnumerable<string> claimTypes)
        {
            if (claimTypes == null) {
                throw new ArgumentNullException(nameof(claimTypes));
            }

            // ReSharper disable once PossibleMultipleEnumeration
            ClaimTypes = claimTypes.ToList()
                         // ReSharper disable once PossibleMultipleEnumeration
                         ?? throw RuntimeInconsistencyException.FromUnexpectedNull(() => claimTypes.ToList());

            if (!ClaimTypes.Any()) {
                throw new InvalidOperationException("Provided list of claim types is empty.");
            }
        }

    }

}
