﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;


namespace XploRe.AspNetCore.Authorization
{

    /// <inheritdoc />
    /// <summary>
    ///     Validates whether any requirement stored by the <see cref="DisjunctiveAuthorizationRequirement" /> instance
    ///     is met.
    /// </summary>
    public class DisjunctiveAuthorizationHandler : AuthorizationHandler<DisjunctiveAuthorizationRequirement>
    {

        /// <inheritdoc />
        [NotNull]
        protected override async Task HandleRequirementAsync(
            [NotNull] AuthorizationHandlerContext context,
            [NotNull] DisjunctiveAuthorizationRequirement disjunction)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (disjunction == null) {
                throw new ArgumentNullException(nameof(disjunction));
            }

            var user = context.User;

            if (user != null) {
                var authorizationService = AuthorizationService.Value;

                foreach (var requirement in disjunction.Requirements) {
                    // ReSharper disable once PossibleNullReferenceException
                    var result = await authorizationService.AuthorizeAsync(user, context.Resource, requirement);

                    if (result?.Succeeded == true) {
                        context.Succeed(disjunction);
                        break;
                    }
                }
            }
        }


        #region (Services)

        // Lazily retrieved to avoid circular dependency.
        [NotNull]
        [ItemNotNull]
        private Lazy<IAuthorizationService> AuthorizationService { get; }

        /// <summary>
        ///     Initialises a new <see cref="DisjunctiveAuthorizationHandler" /> instance.
        /// </summary>
        /// <param name="serviceProvider">
        ///     The <see cref="IServiceProvider" /> instance from which an <see cref="IAuthorizationService" /> instance
        ///     is retrieved to authorise requirements.
        /// </param>
        public DisjunctiveAuthorizationHandler([NotNull] IServiceProvider serviceProvider)
        {
            if (serviceProvider == null) {
                throw new ArgumentNullException(nameof(serviceProvider));
            }

            AuthorizationService = new Lazy<IAuthorizationService>(
                serviceProvider.GetRequiredService<IAuthorizationService>
            );
        }

        #endregion

    }

}
