﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;


namespace XploRe.AspNetCore.Authorization
{

    /// <inheritdoc />
    /// <summary>
    ///     Validates whether the current context user meets at least one policy of a provided
    ///     <see cref="PolicyAuthorizationRequirement" /> instance.
    /// </summary>
    public class PolicyAuthorizationHandler : AuthorizationHandler<PolicyAuthorizationRequirement>
    {

        /// <inheritdoc />
        [NotNull]
        protected override async Task HandleRequirementAsync(
            [NotNull] AuthorizationHandlerContext context,
            [NotNull] PolicyAuthorizationRequirement requirement)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (requirement == null) {
                throw new ArgumentNullException(nameof(requirement));
            }

            var user = context.User;

            if (user != null) {
                var authorizationService = AuthorizationService.Value;

                foreach (var policy in requirement.Policies) {
                    // ReSharper disable once PossibleNullReferenceException
                    var result = await authorizationService.AuthorizeAsync(user, policy);

                    if (result?.Succeeded == true) {
                        context.Succeed(requirement);
                        break;
                    }
                }
            }
        }


        #region (Services)

        // Lazily retrieved to avoid circular dependency.
        [NotNull]
        [ItemNotNull]
        private Lazy<IAuthorizationService> AuthorizationService { get; }

        /// <summary>
        ///     Initialises a new <see cref="PolicyAuthorizationHandler" /> instance.
        /// </summary>
        /// <param name="serviceProvider">
        ///     The <see cref="IServiceProvider" /> instance from which an <see cref="IAuthorizationService" /> instance
        ///     is retrieved to authorise policies.
        /// </param>
        public PolicyAuthorizationHandler([NotNull] IServiceProvider serviceProvider)
        {
            if (serviceProvider == null) {
                throw new ArgumentNullException(nameof(serviceProvider));
            }

            AuthorizationService = new Lazy<IAuthorizationService>(
                serviceProvider.GetRequiredService<IAuthorizationService>
            );
        }

        #endregion

    }

}
