﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Authorization
{

    /// <inheritdoc />
    /// <summary>
    ///     Validates for the current context user the existence of at least one of the specified claim types in an
    ///     <see cref="ClaimTypeAuthorizationRequirement" /> instance.
    /// </summary>
    public class ClaimTypeAuthorizationHandler : AuthorizationHandler<ClaimTypeAuthorizationRequirement>
    {

        /// <inheritdoc />
        [NotNull]
        protected override Task HandleRequirementAsync(
            [NotNull] AuthorizationHandlerContext context,
            [NotNull] ClaimTypeAuthorizationRequirement requirement)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (requirement == null) {
                throw new ArgumentNullException(nameof(requirement));
            }

            var user = context.User;

            if (user != null) {
                var found = requirement.ClaimTypes.Any(claim => user.HasClaim(userClaim => userClaim?.Type == claim));

                if (found) {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask
                   ?? throw RuntimeInconsistencyException.FromUnexpectedNull(() => Task.CompletedTask);
        }

    }

}
