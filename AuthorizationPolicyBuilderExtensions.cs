﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Authorization
{

    /// <summary>
    ///     Extension methods for <see cref="AuthorizationPolicyBuilder" /> instances for additional authorisation 
    ///     requirements.
    /// </summary>
    public static class AuthorizationPolicyBuilderExtensions
    {

        /// <summary>
        ///     Adds a new <see cref="DisjunctiveAuthorizationRequirement" /> instance to the current
        ///     <see cref="AuthorizationPolicyBuilder" /> instance.
        /// </summary>
        /// <param name="builder">This <see cref="AuthorizationPolicyBuilder" /> instance.</param>
        /// <param name="configurePolicy">
        ///     The delegate used to build a policy whose requirements are treated as a disjunction, i.e. only one
        ///     requirement must be met for the policy to be met.
        /// .</param>
        /// <returns>This <see cref="AuthorizationPolicyBuilder" /> instance.</returns>
        [NotNull]
        public static AuthorizationPolicyBuilder RequireAny(
            [NotNull] this AuthorizationPolicyBuilder builder,
            [NotNull] Action<AuthorizationPolicyBuilder> configurePolicy)
        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            if (configurePolicy == null) {
                throw new ArgumentNullException(nameof(configurePolicy));
            }

            if (builder.Requirements == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => builder.Requirements);
            }

            var policy = new AuthorizationPolicyBuilder();
            configurePolicy(policy);

            if (policy.Requirements == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => policy.Requirements);
            }

            builder.Requirements.Add(new DisjunctiveAuthorizationRequirement(policy.Requirements));

            return builder;
        }

        /// <summary>
        ///     Adds a new <see cref="ClaimTypeAuthorizationRequirement" /> instance to the current
        ///     <see cref="AuthorizationPolicyBuilder" /> instance.
        /// </summary>
        /// <param name="builder">This <see cref="AuthorizationPolicyBuilder" /> instance.</param>
        /// <param name="claimTypes">Collection of accepted claim types of which at least one must be present.</param>
        /// <returns>This <see cref="AuthorizationPolicyBuilder" /> instance.</returns>
        [NotNull]
        public static AuthorizationPolicyBuilder RequireClaimType(
            [NotNull] this AuthorizationPolicyBuilder builder,
            [NotNull] [ItemNotNull] params string[] claimTypes)
            => RequireClaimType(builder, (IEnumerable<string>) claimTypes);

        /// <summary>
        ///     Adds a new <see cref="ClaimTypeAuthorizationRequirement" /> instance to the current
        ///     <see cref="AuthorizationPolicyBuilder" /> instance.
        /// </summary>
        /// <param name="builder">This <see cref="AuthorizationPolicyBuilder" /> instance.</param>
        /// <param name="claimTypes">Collection of accepted claim types of which at least one must be present.</param>
        /// <returns>This <see cref="AuthorizationPolicyBuilder" /> instance.</returns>
        [NotNull]
        public static AuthorizationPolicyBuilder RequireClaimType(
            [NotNull] this AuthorizationPolicyBuilder builder,
            [NotNull] [ItemNotNull] IEnumerable<string> claimTypes)
        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            if (claimTypes == null) {
                throw new ArgumentNullException(nameof(claimTypes));
            }

            if (builder.Requirements == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => builder.Requirements);
            }

            builder.Requirements.Add(new ClaimTypeAuthorizationRequirement(claimTypes));

            return builder;
        }

        /// <summary>
        ///     Adds a new <see cref="PolicyAuthorizationRequirement" /> instance to the current
        ///     <see cref="AuthorizationPolicyBuilder" /> instance.
        /// </summary>
        /// <param name="builder">This <see cref="AuthorizationPolicyBuilder" /> instance.</param>
        /// <param name="policies">Collection of policy names of which at least one must be met.</param>
        /// <returns>This <see cref="AuthorizationPolicyBuilder" /> instance.</returns>
        [NotNull]
        public static AuthorizationPolicyBuilder RequirePolicy(
            [NotNull] this AuthorizationPolicyBuilder builder,
            [NotNull] [ItemNotNull] params string[] policies)
            => RequirePolicy(builder, (IEnumerable<string>) policies);

        /// <summary>
        ///     Adds a new <see cref="PolicyAuthorizationRequirement" /> instance to the current
        ///     <see cref="AuthorizationPolicyBuilder" /> instance.
        /// </summary>
        /// <param name="builder">This <see cref="AuthorizationPolicyBuilder" /> instance.</param>
        /// <param name="policies">Collection of policy names of which at least one must be met.</param>
        /// <returns>This <see cref="AuthorizationPolicyBuilder" /> instance.</returns>
        [NotNull]
        public static AuthorizationPolicyBuilder RequirePolicy(
            [NotNull] this AuthorizationPolicyBuilder builder,
            [NotNull] [ItemNotNull] IEnumerable<string> policies)
        {
            if (builder == null) {
                throw new ArgumentNullException(nameof(builder));
            }

            if (policies == null) {
                throw new ArgumentNullException(nameof(policies));
            }

            if (builder.Requirements == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => builder.Requirements);
            }

            builder.Requirements.Add(new PolicyAuthorizationRequirement(policies));

            return builder;
        }

    }

}
